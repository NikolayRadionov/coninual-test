package com.continual.datastorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DataStorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataStorageApplication.class, args);
	}

}
