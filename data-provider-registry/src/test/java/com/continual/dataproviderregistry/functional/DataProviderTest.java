package com.continual.dataproviderregistry.functional;

import com.continual.commons.errorhandlerstarter.handler.EndpointErrorHandler;
import com.continual.commons.teststarter.functional.FunctionalKafkaMongoTest;
import com.continual.commons.teststarter.functional.FunctionalMongoTest;
import com.continual.dataproviderregistry.api.v1.controller.DataProviderController;
import com.continual.dataproviderregistry.model.DataProvider;
import org.apache.catalina.filters.CorsFilter;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class DataProviderTest extends FunctionalKafkaMongoTest {

    @Autowired
    private DataProviderController dataProviderController;

    @Override
    public MockMvc getMockMvc() {
        return MockMvcBuilders.standaloneSetup(dataProviderController)
                .setControllerAdvice(new EndpointErrorHandler())
                .addFilters(new CorsFilter())
                .build();
    }

    @Test
    public void testEndpoints() throws Exception {
        get("/api/v1/data-providers")
                .andExpect(jsonPath("$.content", hasSize(0)));

        final var body = DataProvider.builder()
                .url("http://provider-1:8080/")
                .name("provider-1")
                .location("Europe")
                .build();

        final var contentAsString = post("/api/v1/data-providers", body)
                .andReturn()
                .getResponse()
                .getContentAsString();
        final var postResult = objectMapper.readValue(contentAsString, DataProvider.class);

        get("/api/v1/data-providers/%s".formatted(postResult.getId()))
                .andExpect(jsonPath("$.id").value(postResult.getId()))
                .andExpect(jsonPath("$.url").value(body.getUrl()))
                .andExpect(jsonPath("$.name").value(body.getName()))
                .andExpect(jsonPath("$.location").value(body.getLocation()));
    }
}
