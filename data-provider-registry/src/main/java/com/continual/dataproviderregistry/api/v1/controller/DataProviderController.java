package com.continual.dataproviderregistry.api.v1.controller;

import com.continual.commons.errorhandlerstarter.exception.ApiException;
import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.api.v1.dto.DataProviderDto;
import com.continual.dataproviderregistry.api.v1.dto.FetchDataHookDto;
import com.continual.dataproviderregistry.api.v1.endpoint.DataProviders;
import com.continual.dataproviderregistry.api.v1.mapper.DataProviderMapper;
import com.continual.dataproviderregistry.repository.FetchDataTaskRepository;
import com.continual.dataproviderregistry.service.DataProviderService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.data.domain.Sort.Direction.fromString;

@RestController
@RequiredArgsConstructor
public class DataProviderController implements DataProviders {

    private final DataProviderService dataProviderService;
    private final DataProviderMapper dataProviderMapper;

    @Override
    public ResponseEntity<DataProviderDto> create(DataProviderDto dto) throws ApiException {
        var body = dataProviderMapper.dtoToEntity(dto);
        final var entity = dataProviderService.create(body);
        final var response = dataProviderMapper.entityToDto(entity);
        return ResponseEntity.ok(response);
    }

    @Override
    public ResponseEntity<Page<DataProviderDto>> find(String url, String name, String orderBy, String orderType, Integer offset, Integer limit) {
        final var page = dataProviderService.find(url, name, orderBy, fromString(orderType), offset, limit);
        final var apiDtoPage = dataProviderMapper.entityPageToDtoPage(page);
        return ResponseEntity.ok(apiDtoPage);
    }

    @Override
    public ResponseEntity<DataProviderDto> get(String id) throws ApiException {
        final var api = dataProviderService.get(id);
        final var apiDto = dataProviderMapper.entityToDto(api);
        return ResponseEntity.ok(apiDto);
    }

    @Override
    public ResponseEntity<Void> triggerDataFetchHook(FetchDataHookDto dto) throws ApiException {
        //dataProviderService.createFetch()
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
