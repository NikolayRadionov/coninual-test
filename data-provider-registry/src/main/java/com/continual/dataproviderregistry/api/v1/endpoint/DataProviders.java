package com.continual.dataproviderregistry.api.v1.endpoint;

import com.continual.commons.errorhandlerstarter.exception.ApiException;
import com.continual.commons.mongostarter.model.Page;
import com.continual.commons.webstarter.annotation.ApiV1;
import com.continual.dataproviderregistry.api.v1.dto.DataProviderDto;
import com.continual.dataproviderregistry.api.v1.dto.FetchDataHookDto;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;

@Api(value = "Data Provider Controller")
@RestController
@ApiV1
public interface DataProviders {

    @ApiOperation(value = "Register a Data Provider")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully registered a Data Provider"),
            @ApiResponse(code = 401, message = "You are not authorized to use the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @RequestMapping(path = "/data-providers", method = RequestMethod.POST, produces = APPLICATION_JSON, consumes = APPLICATION_JSON)
    ResponseEntity<DataProviderDto> create(@RequestBody @NotNull @Valid DataProviderDto dto) throws ApiException;

    @ApiOperation(value = "Get an Data Provider by ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved the Data provider"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found")
    })
    @RequestMapping(path = "/data-providers/{id}", method = RequestMethod.GET, produces = APPLICATION_JSON)
    ResponseEntity<DataProviderDto> get(@PathVariable("id") String id) throws ApiException;

    @ApiOperation(value = "Indicate that Data Provider has new data to fetch")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully registered a new data indicator"),
            @ApiResponse(code = 401, message = "You are not authorized to use the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @RequestMapping(path = "/data-providers/data-fetch-hook", method = RequestMethod.POST, produces = APPLICATION_JSON, consumes = APPLICATION_JSON)
    ResponseEntity<Void> triggerDataFetchHook(@RequestBody @NotNull @Valid FetchDataHookDto dto) throws ApiException;

    @ApiOperation(value = "Get list of registered Data Providers")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully retrieved list"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden")
    })
    @RequestMapping(path = "/data-providers", method = RequestMethod.GET, produces = APPLICATION_JSON)
    ResponseEntity<Page<DataProviderDto>> find(
            @ApiParam(value = "URL that need to be considered for filter.")
            @RequestParam(value = "url", required = false) String url,
            @ApiParam(value = "Name that need to be considered for filter.")
            @RequestParam(value = "name", required = false) String name,
            @ApiParam(value = "Sort field for the results")
            @RequestParam(value = "orderBy", required = false, defaultValue = "profile") String orderBy,
            @ApiParam(value = "Sort direction for the results")
            @RequestParam(value = "orderType", required = false, defaultValue = "DESC") String orderType,
            @ApiParam(value = "First index of the results to retrieve")
            @RequestParam(value = "offset", required = false, defaultValue = "0") Integer offset,
            @ApiParam(value = "Maximum count of results to retrieve")
            @RequestParam(value = "limit", required = false, defaultValue = "50") Integer limit);
}
