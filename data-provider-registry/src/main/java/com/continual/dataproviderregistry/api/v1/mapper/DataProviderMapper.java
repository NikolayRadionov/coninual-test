package com.continual.dataproviderregistry.api.v1.mapper;

import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.api.v1.dto.DataProviderDto;
import com.continual.dataproviderregistry.model.DataProvider;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DataProviderMapper {

    public DataProvider dtoToEntity(DataProviderDto dto) {
        if (dto == null) {
            return null;
        }
        return DataProvider.builder()
                .id(dto.getId())
                .name(dto.getName())
                .url(dto.getUrl())
                .location(dto.getLocation())
                .build();
    }

    public DataProviderDto entityToDto(DataProvider entity) {
        if (entity == null) {
            return null;
        }
        return DataProviderDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .url(entity.getUrl())
                .location(entity.getLocation())
                .build();
    }

    public List<DataProviderDto> entityListToDtoList(List<DataProvider> entityList) {
        if (entityList == null) {
            return Collections.emptyList();
        }
        return entityList.stream()
                .map(this::entityToDto)
                .collect(Collectors.toList());
    }

    public Page<DataProviderDto> entityPageToDtoPage(Page<DataProvider> page) {
        final var content = entityListToDtoList(page.getContent());
        return new Page<>(content, page.getTotal(), page.getLimit(), page.getOffset());
    }
}
