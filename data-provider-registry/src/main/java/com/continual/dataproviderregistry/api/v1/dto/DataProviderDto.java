package com.continual.dataproviderregistry.api.v1.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DataProviderDto {

    private String id;
    @NotNull
    private String url;
    @NotNull
    private String name;
    private String location;
}
