package com.continual.dataproviderregistry.eventbus.event;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

@Data
@Builder
@ToString
public class DataProviderStateChangeEvent {
    private String dataProviderId;
    private String dataProviderUrl;
    private DataProviderState state;
}
