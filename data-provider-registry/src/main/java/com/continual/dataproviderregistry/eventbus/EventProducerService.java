package com.continual.dataproviderregistry.eventbus;

import com.continual.dataproviderregistry.eventbus.event.DataProviderState;
import com.continual.dataproviderregistry.eventbus.event.DataProviderStateChangeEvent;
import com.continual.dataproviderregistry.model.DataProvider;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@ConditionalOnProperty("spring.kafka.enabled")
public class EventProducerService {

    private static final Logger log = LoggerFactory.getLogger(EventProducerService.class);

    private final ObjectMapper mapper;
    private final KafkaTemplate<String, String> kafkaTemplate;

    private String dataProviderStateChangeTopic;

    @Value(value = "${eventbus.topics.data-provider-state-change}")
    public void setDataProviderUpdatesTopic(String dataProviderStateChangeTopic) {
        this.dataProviderStateChangeTopic = dataProviderStateChangeTopic;
    }

    public void sendDataProviderStateChangeEvent(String providerId, String url, DataProviderState state) {
        if (StringUtils.isEmpty(providerId)) {
            log.error("Data Provider id is empty");
            throw new RuntimeException("Cannot send event. Data Provider ID is empty");
        }
        if (StringUtils.isEmpty(url)) {
            log.error("Data Provider URL is empty");
            throw new RuntimeException("Cannot send event. Data Provider URL is empty");
        }
        final var event = DataProviderStateChangeEvent.builder()
                .dataProviderId(providerId)
                .dataProviderUrl(url)
                .state(state)
                .build();
        final String jsonBody;
        try {
            jsonBody = mapper.writeValueAsString(event);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Cannot send serialize DataProviderStateChangeEvent", e);
        }
        kafkaTemplate.send(dataProviderStateChangeTopic, jsonBody);
        log.debug("Sent DataProviderStateChangeEvent {}", event);
    }
}
