package com.continual.dataproviderregistry.model;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.OffsetDateTime;

@Data
@Builder
@Document(collection = "fetch_data_tasks")
public class FetchDataTask {
    @Id
    private String id;
    private String providerId;
    private String url;
    private FetchDataIndicatorStatus status;
    private OffsetDateTime created;
    private OffsetDateTime modifiedDate;
}
