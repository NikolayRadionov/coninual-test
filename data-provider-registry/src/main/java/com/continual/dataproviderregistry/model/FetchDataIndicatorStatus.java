package com.continual.dataproviderregistry.model;

public enum FetchDataIndicatorStatus {
    SCHEDULED, PROCESSING, COMPLETED
}
