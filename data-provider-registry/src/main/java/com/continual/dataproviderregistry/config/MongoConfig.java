package com.continual.dataproviderregistry.config;

import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.util.StringUtils;

@Configuration
@EnableMongoRepositories(basePackages = {"com.continual.dataproviderregistry"})
public class MongoConfig {

    private String uri;
    private String hostname;
    private String databaseName;
    private String username;
    private String password;
    private int port;

    @Value("${spring.data.mongodb.host:mongodb}")
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }
    @Value("${spring.data.mongodb.database:data-provider-registry}")
    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }
    @Value("${spring.data.mongodb.username:continual}")
    public void setUsername(String username) {
        this.username = username;
    }
    @Value("${spring.data.mongodb.password:bWloNjBmV29mUCtWQ0pIU3l1TVR3L2tNZ3d}")
    public void setPassword(String password) {
        this.password = password;
    }
    @Value("${spring.data.mongodb.port:27017}")
    public void setPort(int port) {
        this.port = port;
    }

    @Value("${spring.data.mongodb.uri:}")
    public void setUri(String uri) {
        this.uri = uri;
    }

    public String mongoClientURI() {
        return StringUtils.hasText(uri) ? uri : String.format("mongodb://%s:%s@%s:%s/%s?authSource=admin&readPreference=primary&ssl=false", username, password, hostname, port, databaseName);
    }

    @Bean
    public MongoClient mongo() {
        ConnectionString connectionString = new ConnectionString(mongoClientURI());
        MongoClientSettings mongoClientSettings = MongoClientSettings.builder()
                .applyConnectionString(connectionString)
                .build();

        return MongoClients.create(mongoClientSettings);
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), databaseName);
    }
}