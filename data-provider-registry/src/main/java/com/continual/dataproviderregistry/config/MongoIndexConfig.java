package com.continual.dataproviderregistry.config;

import com.continual.dataproviderregistry.model.DataProvider;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.Index;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Configuration
@RequiredArgsConstructor
@ConditionalOnBean(MongoTemplate.class)
public class MongoIndexConfig {

    private static final Logger log = LoggerFactory.getLogger(MongoIndexConfig.class);
    private final MongoTemplate mongoTemplate;

    @PostConstruct
    public void initIndexes() {
        log.info("Configuring Mongo Indexes");

        final var indexAppIdInOpenApiData = mongoTemplate.indexOps(DataProvider.class);
        Arrays.asList("url", "name").forEach(field -> {
            indexAppIdInOpenApiData.ensureIndex(new Index().on(field, Sort.Direction.ASC).unique());
        });

    }

}