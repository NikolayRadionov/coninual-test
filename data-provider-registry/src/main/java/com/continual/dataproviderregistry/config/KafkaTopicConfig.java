package com.continual.dataproviderregistry.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@ConditionalOnProperty("spring.kafka.enabled")
public class KafkaTopicConfig {

    private String dataProviderStateChangeTopic;

    @Value(value = "${eventbus.topics.data-provider-state-change}")
    public void setDataProviderUpdatesTopic(String dataProviderStateChangeTopic) {
        this.dataProviderStateChangeTopic = dataProviderStateChangeTopic;
    }

    @Bean
    public NewTopic dataProviderUpdatesTopic() {
        return TopicBuilder.name(dataProviderStateChangeTopic)
                .partitions(1)
                .replicas(1)
                .build();
    }

}
