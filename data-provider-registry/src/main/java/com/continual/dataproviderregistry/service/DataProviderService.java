package com.continual.dataproviderregistry.service;

import com.continual.commons.errorhandlerstarter.exception.NotFoundException;
import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.model.DataProvider;
import com.continual.dataproviderregistry.model.FetchDataIndicatorStatus;
import com.continual.dataproviderregistry.model.FetchDataTask;
import com.continual.dataproviderregistry.repository.DataProviderRepository;
import com.continual.dataproviderregistry.repository.FetchDataTaskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
@RequiredArgsConstructor
public class DataProviderService {

    private final DataProviderRepository repository;
    private final FetchDataTaskRepository fetchDataTaskRepository;

    public Page<DataProvider> find(String url, String name, String orderBy, Sort.Direction orderType, Integer offset, Integer limit) {
        return repository.find(url, name, orderBy, orderType, offset, limit);
    }

    public DataProvider get(String id) throws NotFoundException {
        return repository.findById(id)
                .orElseThrow(() -> new NotFoundException("API item not found in DB. Id=" + id));
    }

    public DataProvider create(DataProvider body) {
        final var offsetDateTime = OffsetDateTime.now();
        body.setCreated(offsetDateTime);
        body.setModifiedDate(offsetDateTime);
        final var dbRecord = repository.save(body);

        final var fetchDataTask = FetchDataTask.builder()
                .providerId(dbRecord.getId())
                .url(dbRecord.getUrl())
                .status(FetchDataIndicatorStatus.SCHEDULED)
                .created(offsetDateTime)
                .modifiedDate(offsetDateTime)
                .build();

        fetchDataTaskRepository.save(fetchDataTask);
        return dbRecord;
    }
}
