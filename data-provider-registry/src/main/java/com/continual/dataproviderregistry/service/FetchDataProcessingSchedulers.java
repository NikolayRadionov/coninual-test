package com.continual.dataproviderregistry.service;

import com.continual.dataproviderregistry.eventbus.EventProducerService;
import com.continual.dataproviderregistry.eventbus.event.DataProviderState;
import com.continual.dataproviderregistry.model.FetchDataTask;
import com.continual.dataproviderregistry.repository.FetchDataTaskRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.OffsetDateTime;
import java.time.temporal.ChronoUnit;

import static com.continual.dataproviderregistry.eventbus.event.DataProviderState.HAS_NEW_DATA;

@Component
@RequiredArgsConstructor
public class FetchDataProcessingSchedulers {

    private static final Logger log = LoggerFactory.getLogger(FetchDataProcessingSchedulers.class);

    private long processingTaskTimeout;

    private final FetchDataTaskRepository fetchDataTaskRepository;
    private final EventProducerService eventProducerService;

    @Value("${fetch-data-processing.running-task-timeout:180000}")
    public void setProcessingTaskTimeout(long processingTaskTimeout) {
        this.processingTaskTimeout = processingTaskTimeout;
    }

    @Scheduled(fixedDelayString = "${fetch-data-processing.check-schedule-interval:10000}")
    public void checkScheduledTasks() {
        rescheduleTimedOutTasks();

        FetchDataTask fetchDataTask;
        do {
            fetchDataTask = fetchDataTaskRepository.findAndLockForProcessing();
            eventProducerService.sendDataProviderStateChangeEvent(fetchDataTask.getProviderId(), fetchDataTask.getUrl(), HAS_NEW_DATA);
            fetchDataTaskRepository.markTaskAsCompleted(fetchDataTask);
        } while (fetchDataTask != null);
    }

    /**
     * Reschedules timed out fetch data tasks which stuck in PROCESSING state.
     */
    private void rescheduleTimedOutTasks() {
        FetchDataTask fetchDataTask;
        do {
            fetchDataTask = fetchDataTaskRepository.findAndRescheduleOldestTimedOutTask(
                    OffsetDateTime.now().minus(processingTaskTimeout, ChronoUnit.MILLIS));
            if (fetchDataTask != null) {
                log.info("Timed out running Fetch Data Task has been moved to SCHEDULED state: {}", fetchDataTask);
            }
        } while (fetchDataTask != null);
    }
}
