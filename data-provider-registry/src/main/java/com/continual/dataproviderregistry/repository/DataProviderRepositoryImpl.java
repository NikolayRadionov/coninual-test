package com.continual.dataproviderregistry.repository;


import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.model.DataProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import static com.continual.commons.mongostarter.helpers.MongoHelper.*;

@Repository
@RequiredArgsConstructor
public class DataProviderRepositoryImpl implements DataProviderRepositoryExtended {

    private final MongoTemplate mongoTemplate;

    @Override
    public Page<DataProvider> find(String url, String name, String orderBy, Sort.Direction orderType, Integer offset, Integer limit) {
        Criteria criteria = new Criteria();

        filterIfValueIsNotNull(criteria, "url", url);
        filterIfValueIsNotNull(criteria, "name", name);

        final var pageable = createPageable(orderBy, orderType, offset, limit);
        return getPage(mongoTemplate, pageable, Query.query(criteria), DataProvider.class);
    }
}
