package com.continual.dataproviderregistry.repository;

import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.model.DataProvider;
import com.continual.dataproviderregistry.model.FetchDataTask;
import com.continual.dataproviderregistry.model.FetchDataIndicatorStatus;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;

import static com.continual.commons.mongostarter.helpers.MongoHelper.*;

@Repository
@RequiredArgsConstructor
public class FetchDataTaskRepositoryImpl implements FetchDataTaskRepositoryExtended {

    private final MongoTemplate mongoTemplate;

    @Override
    public FetchDataTask findAndLockForProcessing() {
        Query query = Query.query(Criteria.where("status").is(FetchDataIndicatorStatus.SCHEDULED)
                .and("modifiedDate").lte(OffsetDateTime.now()))
                .with(Sort.by(Sort.Direction.ASC, "modifiedDate"))
                .limit(1);

        Update update = new Update();
        update.set("status", FetchDataIndicatorStatus.PROCESSING);
        update.set("modifiedDate", OffsetDateTime.now());

        return mongoTemplate.findAndModify(query,
                update,
                FindAndModifyOptions.options().returnNew(true),
                FetchDataTask.class);
    }

    @Override
    public void markTaskAsCompleted(FetchDataTask fetchDataTask) {
        Query query = Query.query(Criteria.where("id").is(fetchDataTask.getId())
                .and("status").is(FetchDataIndicatorStatus.PROCESSING));

        Update update = Update.update("status", FetchDataIndicatorStatus.COMPLETED);

        mongoTemplate.findAndModify(query,
                update,
                FindAndModifyOptions.options().returnNew(true),
                FetchDataTask.class);
    }

    @Override
    public FetchDataTask findAndRescheduleOldestTimedOutTask(OffsetDateTime lastUpdateTime) {
        Query query = Query.query(Criteria
                .where("status").is(FetchDataIndicatorStatus.PROCESSING)
                .and("modifiedDate").lte(lastUpdateTime))
                .with(Sort.by(Sort.Direction.ASC, "modifiedDate"))
                .limit(1);

        Update update = Update
                .update("state", FetchDataIndicatorStatus.SCHEDULED);

        return mongoTemplate.findAndModify(
                query, update,
                FindAndModifyOptions.options().returnNew(true),
                FetchDataTask.class);
    }
}
