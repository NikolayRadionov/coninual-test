package com.continual.dataproviderregistry.repository;

import com.continual.dataproviderregistry.model.DataProvider;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DataProviderRepository extends MongoRepository<DataProvider, String>, DataProviderRepositoryExtended {
    Optional<DataProvider> findById(String id);
}
