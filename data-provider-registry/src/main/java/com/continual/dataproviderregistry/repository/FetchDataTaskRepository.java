package com.continual.dataproviderregistry.repository;

import com.continual.dataproviderregistry.model.FetchDataTask;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FetchDataTaskRepository extends MongoRepository<FetchDataTask, String>, FetchDataTaskRepositoryExtended {
    Optional<FetchDataTask> findById(String id);
}
