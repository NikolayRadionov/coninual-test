package com.continual.dataproviderregistry.repository;

import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.model.DataProvider;
import com.continual.dataproviderregistry.model.FetchDataTask;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

import java.time.OffsetDateTime;

@Repository
public interface FetchDataTaskRepositoryExtended {
    FetchDataTask findAndLockForProcessing();
    FetchDataTask findAndRescheduleOldestTimedOutTask(OffsetDateTime lastUpdateTime);
    void markTaskAsCompleted(FetchDataTask fetchDataTask);
}
