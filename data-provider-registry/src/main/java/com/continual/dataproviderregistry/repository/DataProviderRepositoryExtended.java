package com.continual.dataproviderregistry.repository;

import com.continual.commons.mongostarter.model.Page;
import com.continual.dataproviderregistry.model.DataProvider;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Repository;

@Repository
public interface DataProviderRepositoryExtended {
    Page<DataProvider> find(String url, String name, String orderBy, Sort.Direction orderType, Integer offset, Integer limit);
}
