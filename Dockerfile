FROM openjdk:16-jdk-alpine
ARG microservice
ARG version
COPY "target/$microservice-$version.jar" "$microservice-$version.jar"
ENTRYPOINT ["sh", "-c", "java -Djava.security.egd=file:/dev/./urandom -jar $(printf %s *.jar)"]