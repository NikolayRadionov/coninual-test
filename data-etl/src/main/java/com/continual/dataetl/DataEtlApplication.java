package com.continual.dataetl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class DataEtlApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataEtlApplication.class, args);
	}

}
