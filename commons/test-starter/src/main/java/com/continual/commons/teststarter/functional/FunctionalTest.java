package com.continual.commons.teststarter.functional;

import com.continual.commons.teststarter.category.FunctionalTestCategory;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Before;
import org.junit.experimental.categories.Category;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ActiveProfiles("functional")
@Category(FunctionalTestCategory.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
	properties = {"spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration"}
)
public abstract class FunctionalTest {

	protected MockMvc mockMvc;
	protected MockRestServiceServer mockServer;
	protected ObjectMapper objectMapper;

	@Autowired
	private RestTemplate restTemplate;

	public abstract MockMvc getMockMvc();

	@BeforeEach
	public void setup() throws Exception {
		this.mockMvc = getMockMvc();
		this.mockServer = MockRestServiceServer.createServer(this.restTemplate);
	}

	@AfterEach
	public void teardown() throws Exception {
		this.mockMvc = null;
		this.mockServer = null;
	}

	@Autowired
	public void setObjectMapper(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public ResultActions get(final String url) throws Exception {
		return doGet(url)
				.andExpect(status().isOk());
	}

	public ResultActions get400(final String url) throws Exception {
		return doGet(url)
				.andExpect(status().isBadRequest());
	}

	public ResultActions get404(final String url) throws Exception {
		return doGet(url)
				.andExpect(status().isNotFound());
	}

	public ResultActions get500(final String url) throws Exception {
		return doGet(url)
				.andExpect(status().isInternalServerError());
	}

	public ResultActions post(final String url, Object body) throws Exception {
		return postJson(url, body)
				.andExpect(status().isOk());
	}

	public ResultActions post204(final String url, Object body) throws Exception {
		return postJson(url, body)
				.andExpect(status().isCreated());
	}

	public ResultActions post400(final String url, Object body) throws Exception {
		return postJson(url, body)
				.andExpect(status().isBadRequest());
	}

	public ResultActions post404(final String url, Object body) throws Exception {
		return postJson(url, body)
				.andExpect(status().isNotFound());
	}

	public ResultActions post500(final String url, Object body) throws Exception {
		return postJson(url, body)
				.andExpect(status().isInternalServerError());
	}

	public ResultActions put(final String url, Object body) throws Exception {
		return putJson(url, body)
				.andExpect(status().isOk());
	}

	public ResultActions put204(final String url, Object body) throws Exception {
		return putJson(url, body)
				.andExpect(status().isCreated());
	}

	public ResultActions put400(final String url, Object body) throws Exception {
		return putJson(url, body)
				.andExpect(status().isBadRequest());
	}

	public ResultActions put404(final String url, Object body) throws Exception {
		return putJson(url, body)
				.andExpect(status().isNotFound());
	}

	public ResultActions put500(final String url, Object body) throws Exception {
		return putJson(url, body)
				.andExpect(status().isInternalServerError());
	}

	public ResultActions delete(final String url) throws Exception {
		return deleteJson(url)
				.andExpect(status().isOk());
	}

	public ResultActions delete204(final String url) throws Exception {
		return deleteJson(url)
				.andExpect(status().isCreated());
	}

	public ResultActions delete400(final String url) throws Exception {
		return deleteJson(url)
				.andExpect(status().isBadRequest());
	}

	public ResultActions delete404(final String url) throws Exception {
		return deleteJson(url)
				.andExpect(status().isNotFound());
	}

	public ResultActions delete500(final String url) throws Exception {
		return deleteJson(url)
				.andExpect(status().isInternalServerError());
	}

	public ResultActions postJson(String url, Object body) throws Exception {
		String bodyString = body instanceof String ? (String) body : objectMapper.writeValueAsString(body);
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post(url)
				.content(bodyString)
				.contentType(MediaType.APPLICATION_JSON);

		return mockMvc.perform(builder)
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}

	public ResultActions putJson(String url, Object body) throws Exception {
		String bodyString = body instanceof String ? (String) body : objectMapper.writeValueAsString(body);
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.put(url)
				.content(bodyString)
				.contentType(MediaType.APPLICATION_JSON);

		return mockMvc.perform(builder)
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}

	public ResultActions deleteJson(String url) throws Exception {
		return doDelete(url)
				.andExpect(content().contentType(MediaType.APPLICATION_JSON));
	}

	public ResultActions doDelete(String url) throws Exception {
		MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.delete(url)
				.contentType(MediaType.APPLICATION_JSON);

		return mockMvc.perform(builder)
				.andDo(print());
	}

	public ResultActions doGet(final String url) throws Exception {
		return mockMvc.perform(MockMvcRequestBuilders.get(url)
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print());
	}

}
