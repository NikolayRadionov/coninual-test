package com.continual.commons.teststarter.functional;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.util.TestPropertyValues;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.HttpWaitStrategy;
import org.testcontainers.utility.Base58;

import java.time.Duration;

import static java.net.HttpURLConnection.HTTP_OK;
import static java.net.HttpURLConnection.HTTP_UNAUTHORIZED;

@DirtiesContext
@ContextConfiguration(initializers = FunctionalMongoTest.EnvInitializer.class)
public abstract class FunctionalMongoTest extends FunctionalTest {

    protected static GenericContainer<?> mongoContainer;

    @Autowired
    protected MongoTemplate mongoTemplate;

    @Autowired
    protected ObjectMapper objectMapper;

    @BeforeAll
    public static void initContainers() {
        mongoContainer = new GenericContainer<>("mongo:4.0")
                .withNetworkAliases("mongo-" + Base58.randomString(6))
                .withExposedPorts(27017)
                .waitingFor(new HttpWaitStrategy()
                        .forPort(27017)
                        .forStatusCodeMatching(response -> response == HTTP_OK || response == HTTP_UNAUTHORIZED)
                        .withStartupTimeout(Duration.ofMinutes(2)));
        mongoContainer.start();
    }

    @AfterAll
    public static void stopContainers() {
        if (mongoContainer != null) {
            mongoContainer.stop();
        }
    }

    @BeforeEach
    public void setup() throws Exception {
        cleanupDbCollections();
        super.setup();
    }

    @AfterEach
    public void teardown() throws Exception {
        this.cleanupDbCollections();
        super.teardown();
    }

    public static class EnvInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
            final var mappedPort = mongoContainer.getMappedPort(27017);
            TestPropertyValues.of(
                    "spring.data.mongodb.uri=" + String.format("mongodb://localhost:%s/test_db", mappedPort)
            ).applyTo(applicationContext);
        }
    }

    protected void cleanupDbCollections() {
        mongoTemplate.getDb()
                .listCollectionNames()
                .forEach(mongoTemplate::dropCollection);
    }

}
