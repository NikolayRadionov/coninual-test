package com.continual.commons.mongostarter.model;

import org.springframework.data.domain.AbstractPageRequest;
import org.springframework.data.domain.Sort;

import static org.springframework.data.domain.Sort.unsorted;

public class MongoPageable extends AbstractPageRequest {

    private final Sort sort;
    private final int offset;

    private MongoPageable(int offset, int limit, Sort sort) {
        super(offset / limit, limit);
        this.offset = offset;
        this.sort = sort;
    }

    public static MongoPageable of(int offset, int limit, Sort sort) {
        return new MongoPageable(offset, limit, sort);
    }

    public static MongoPageable of(int offset, int limit) {
        return new MongoPageable(offset, limit, unsorted());
    }

    @Override
    public Sort getSort() {
        return this.sort;
    }

    @Override
    public long getOffset() {
        return this.offset;
    }

    @Override
    public MongoPageable first() {
        return new MongoPageable(0, getPageSize(), getSort());
    }

    @Override
    public MongoPageable next() {
        return new MongoPageable(this.offset + getPageSize(), getPageSize(), getSort());
    }

    @Override
    public MongoPageable previous() {
        return getPageNumber() == 0 ? this : new MongoPageable(this.offset - getPageSize(), getPageSize(), getSort());
    }
}