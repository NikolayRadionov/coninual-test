package com.continual.commons.mongostarter.helpers;

import com.continual.commons.mongostarter.model.MongoPageable;
import com.continual.commons.mongostarter.model.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.support.PageableExecutionUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.lang.String.format;

@Component
public class MongoHelper {

    static final int PAGE_LIMIT = 50;

    public static Pattern valueToRegex(String value) {
        return Pattern.compile("^" + value.replaceAll("\\*", ".*") + "$", Pattern.CASE_INSENSITIVE);
    }

    public static List<Object> valueListToRegex(List<String> items) {
        return items.stream().map(MongoHelper::valueToRegex).collect(Collectors.toList());
    }

    public static void filterIfValueIsNotNull(Criteria criteria, String key, Object value) {
        if (value != null) {
            criteria.and(key).is(value);
        }
    }

    public static void filterByValueRegexIn(Criteria criteria, String key, List<?> values) {
        if (!CollectionUtils.isEmpty(values) && !values.contains("*")) {
            criteria.and(key).in(valueListToRegex(values.stream().filter(Objects::nonNull).map(Object::toString).collect(Collectors.toList())));
        }
    }

    public static void filterByValueIn(Criteria criteria, String key, List<?> values) {
        if (!CollectionUtils.isEmpty(values)) {
            criteria.and(key).in(values.stream().filter(Objects::nonNull).map(Object::toString).collect(Collectors.toList()));
        }
    }

    public static <T> Page<T> getPage(MongoTemplate mongoTemplate, Pageable pageRequest, Query query, Class<T> clazz) {
        final var count = mongoTemplate.count(query, clazz);
        final List<T> list = mongoTemplate.find(query.with(pageRequest), clazz);
        final org.springframework.data.domain.Page page = PageableExecutionUtils.getPage(list, pageRequest, () -> count);
        return new Page<T>(page);
    }

    public static MongoPageable createPageable(String orderBy, Sort.Direction orderType, Integer offset, Integer limit, Integer pageLimit, Function<String, RuntimeException> exceptionFunction, Map<String, String> orderByFieldsMap) {
        if (limit > pageLimit || limit <= 0 || offset < 0) {
            throw exceptionFunction.apply(format("Page parameters are not valid: limit: %s, offset: %s", offset, limit));
        }
        final var orderByString = orderByFieldsMap.getOrDefault(orderBy, orderBy);
        final var sortBy = Sort.by(orderType, orderByString);
        return MongoPageable.of(offset, limit, sortBy);
    }

    public static MongoPageable createPageable(String orderBy, Sort.Direction orderType, Integer offset, Integer limit, Function<String, RuntimeException> exceptionFunction) {
        return createPageable(orderBy, orderType, offset, limit, PAGE_LIMIT, exceptionFunction, Collections.emptyMap());
    }

    public static MongoPageable createPageable(String orderBy, Sort.Direction orderType, Integer offset, Integer limit) {
        return createPageable(orderBy, orderType, offset, limit, PAGE_LIMIT, IllegalArgumentException::new, Collections.emptyMap());
    }
}
