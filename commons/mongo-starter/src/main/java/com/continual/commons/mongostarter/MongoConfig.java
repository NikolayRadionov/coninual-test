package com.continual.commons.mongostarter;

import com.continual.commons.mongostarter.converters.DateToOffsetDateTimeConverter;
import com.continual.commons.mongostarter.converters.OffsetDateTimeToDateConverter;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;

import java.util.Arrays;

@Configuration
public class MongoConfig {

    static final String MAP_KEY_DOT_REPLACEMENT = "_dot_repl_";

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory, ObjectMapper mapper) throws Exception {
        return new MongoTemplate(mongoDbFactory, getDefaultMongoConverter(mongoDbFactory, mapper));
    }

    @Bean
    public MongoCustomConversions customConversions(ObjectMapper mapper) {
        return new MongoCustomConversions(Arrays.asList(
                new OffsetDateTimeToDateConverter(),
                new DateToOffsetDateTimeConverter())
        );
    }

    @Bean
    public MappingMongoConverter getDefaultMongoConverter(MongoDbFactory mongoDbFactory, ObjectMapper mapper) throws Exception {
        MappingMongoConverter converter = new MappingMongoConverter(
                new DefaultDbRefResolver(mongoDbFactory), new MongoMappingContext());
        converter.setCustomConversions(customConversions(mapper));
        converter.setMapKeyDotReplacement(MAP_KEY_DOT_REPLACEMENT);
        return converter;
    }
}
