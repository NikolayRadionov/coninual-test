package com.continual.commons.mongostarter.model;

import java.util.List;
import java.util.Objects;

public class Page<T> {

    private List<T> content;
    private long total;
    private long limit;
    private long offset;

    public Page(List<T> content, long total, long limit, long offset) {
        this.content = content;
        this.total = total;
        this.limit = limit;
        this.offset = offset;
    }

    public Page(org.springframework.data.domain.Page<T> page) {
        this(page.getContent(), page.getTotalElements(), page.getSize(), page.getPageable().getOffset());
    }

    public List<T> getContent() {
        return content;
    }

    public long getTotal() {
        return total;
    }

    public long getLimit() {
        return limit;
    }

    public long getOffset() {
        return offset;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Page<?> page = (Page<?>) o;
        return total == page.total && limit == page.limit && offset == page.offset && Objects.equals(content, page.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(content, total, limit, offset);
    }
}
