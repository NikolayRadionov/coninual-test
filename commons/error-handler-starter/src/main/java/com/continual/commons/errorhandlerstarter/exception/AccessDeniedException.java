package com.continual.commons.errorhandlerstarter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.FORBIDDEN)
public class AccessDeniedException extends ApiException{
    public AccessDeniedException(String message) {
        super(message, HttpStatus.FORBIDDEN.value());
    }

    public AccessDeniedException(Throwable cause) {
        super(cause, HttpStatus.FORBIDDEN.value());
    }
}
