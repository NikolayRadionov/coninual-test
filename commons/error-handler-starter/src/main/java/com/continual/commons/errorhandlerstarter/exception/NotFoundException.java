package com.continual.commons.errorhandlerstarter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotFoundException extends ApiException {

    public NotFoundException(String message) {
        super(message, HttpStatus.NOT_FOUND.value());
    }

    public NotFoundException(Throwable cause) {
        super(cause, HttpStatus.NOT_FOUND.value());
    }
}
