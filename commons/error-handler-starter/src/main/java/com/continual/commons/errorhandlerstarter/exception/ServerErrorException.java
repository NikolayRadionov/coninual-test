package com.continual.commons.errorhandlerstarter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
public class ServerErrorException extends ApiException {

    public ServerErrorException(String message) {
        super(message, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }

    public ServerErrorException(Throwable cause) {
        super(cause, HttpStatus.INTERNAL_SERVER_ERROR.value());
    }
}
