package com.continual.commons.errorhandlerstarter.handler;

import com.continual.commons.errorhandlerstarter.exception.AccessDeniedException;
import com.continual.commons.errorhandlerstarter.exception.ApiException;
import com.continual.commons.errorhandlerstarter.exception.BadRequestException;
import org.apache.tomcat.util.ExceptionUtils;
import org.springframework.boot.web.servlet.error.DefaultErrorAttributes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
public class EndpointErrorHandler extends ResponseEntityExceptionHandler {

    private DefaultErrorAttributes errorParser = new DefaultErrorAttributes();

    @ExceptionHandler
    public final ResponseEntity<Object> handleAnyException(Exception ex, WebRequest request) {
        if (ex instanceof ApiException) {
            HttpStatus status = HttpStatus.resolve(((ApiException) ex).getCode());
            return handleExceptionInternal(ex, null, new HttpHeaders(), status, request);
        }
        if (ex instanceof BadRequestException) {
            HttpStatus status = HttpStatus.BAD_REQUEST;
            return handleExceptionInternal(ex, null, new HttpHeaders(), status, request);
        }
        if (ex instanceof AccessDeniedException) {
            HttpStatus status = HttpStatus.FORBIDDEN;
            return handleExceptionInternal(ex, null, new HttpHeaders(), status, request);
        }
        try {
            return super.handleException(ex, request);
        } catch (Exception e) {
            HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
            return handleExceptionInternal(ex, null, new HttpHeaders(), status, request);
        }
    }

    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object ignore, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Map<String, Object> defaultError = errorParser.getErrorAttributes(request, false);
        defaultError.put("message", this.getRootCauseMessage(ex));
        defaultError.put("exception", ex.getClass());
        defaultError.put("status", status.value());

        if (ex instanceof ApiException) {
            defaultError.put("error", status.getReasonPhrase());
            logger.error("Default error handler: " + ex.getMessage());
        } else if (ex instanceof AccessDeniedException) {
            defaultError.put("error", status.getReasonPhrase());
            defaultError.put("message", "You are not granted to access this resource!");
            logger.error("Default error handler: " + ex.getMessage());
        } else if (ex instanceof BadRequestException) {
            defaultError.put("error", status.getReasonPhrase());
            defaultError.put("message", "Validation failed for argument");
            BindingResult bindingResult = ((MethodArgumentNotValidException) ex).getBindingResult();
            List<MethodArgumentError> errors = bindingResult.getFieldErrors()
                    .stream().map(MethodArgumentError::new).collect(Collectors.toList());
            defaultError.put("errors", errors);
            logger.error("Default error handler: " + ex.getMessage());
        }
        else {
            logger.error("Default error handler ", ex);
        }
        return new ResponseEntity<>(defaultError, headers, status);
    }

    private String getRootCauseMessage(Throwable t) {
        Throwable rootCause = ExceptionUtils.unwrapInvocationTargetException(t); //get root cause
        if (rootCause == null) {
            rootCause = t;
        }
        return rootCause.getMessage();
    }

    static class MethodArgumentError {

        public String field;

        public String message;

        public Object rejectedValue;

        public MethodArgumentError(FieldError fieldError) {
            this.field = fieldError.getField();
            this.message = fieldError.getDefaultMessage();
            this.rejectedValue = fieldError.getRejectedValue();
        }
    }
}
