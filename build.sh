#
# Copyright (c) 2021 Nikolay Radionov. All rights reserved.
#

mvn -N versions:update-child-modules -f ./commons/pom.xml -s /Users/nradionov/.m2/radivel/settings.xml && \
mvn clean install -f ./commons/pom.xml -s /Users/nradionov/.m2/radivel/settings.xml  && \

mvn -N versions:update-child-modules -s /Users/nradionov/.m2/radivel/settings.xml && \
mvn clean package -s /Users/nradionov/.m2/radivel/settings.xml -DskipTests && \
VERSION=$(cat $(pwd)/pom.xml | grep "<version>.*</version>$" | head | awk -F'[><]' '{print $3}' | head -n1) && \

docker build --tag continual/mongo:latest - < Dockerfile-mongo

microservices_dirs=(
"service-discovery"
"service-configuration"
"service-security"
"data-provider-registry"
"data-etl"
"data-storage"
)
for f in "${microservices_dirs[@]}"; do
    if [ -d "$f" ]; then
        echo "================> building docker image: "continual/$f:$VERSION" <================"
        docker build -f ./Dockerfile --tag continual/$f:$VERSION --tag continual/$f:latest --rm --build-arg version=$VERSION  --build-arg microservice=$f $f
        echo $'\n\n'
    fi
done